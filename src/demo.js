import React from 'react';
import { render } from 'react-dom';
import ReactPhoneInput from './index';


class Demo extends React.Component {
  state = { defaultCountry: 'br', value: '12345' }

  render() {
    return (
      <div style={{ fontFamily: "'Roboto', sans-serif", fontSize: '15px' }}>
        <style dangerouslySetInnerHTML={{__html: `
          input[type="tel"].custom-phone-input {
            font-size: 14px;
            border-color: #a0a0a0;
          }

          .custom-phone-button {
            background: rgb(200, 215, 225) !important;
            border-color: #a0a0a0 !important;
          }

          .custom-dropdown {
            margin-top: 0 !important;
          }
        `}} />

        <div style={{ display: 'inline-block', marginLeft: '40px' }}>
          <p>v2</p>

          <p>Disabled area codes with disableAreaCodes</p>
          <ReactPhoneInput
            defaultCountry='us'
            disableAreaCodes
            formattedNumber={false}
            disableCountryCode
            autoFormat={false}
            placeholder={"请输入手机号"}
          />

        </div>

      </div>
    )
  }
}


export default render(
  <Demo />,
  document.getElementById('root')
);
